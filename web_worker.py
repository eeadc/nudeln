#!/usr/bin/env python
# coding: utf-8

import argparse

argparser = argparse.ArgumentParser(description="web worker")

argparser.add_argument("-t", "--socket-type", choices=["unix", "tcp"], default="tcp")
argparser.add_argument("-b", "--bind", type=str, default="::1")
argparser.add_argument("-p", "--port", type=int, default=8888)
argparser.add_argument("-s", "--socket", type=str, default="web_worker.sock")
argparser.add_argument("-m", "--mode", type=int, default=0o666)

argparser.add_argument("-D", "--db-uri", type=str, default="mongodb://localhost")
argparser.add_argument("-d", "--db-name", type=str, default="coqio")

argparser.add_argument("-c", "--config", type=str, default=None)
argparser.add_argument("-v", "--verbose", action="store_true", default=False)

import pymongo
import pymongo.uri_parser
import tornado.httpserver
import tornado.netutil
import tornado.ioloop

def init_database(db_uri, db_name):
	conn = pymongo.Connection(db_uri)
	return conn[db_name]

if __name__ == '__main__':
	args = argparser.parse_args()

	sockets = []

	if args.socket_type == "unix":
		sockets.append(tornado.netutil.bind_unix_socket(args.socket, args.mode))
	elif args.socket_type == "tcp":
		sockets = tornado.netutil.bind_sockets(args.port, args.bind)

	import nudelnio

	nudelnio.config["database"] = init_database(args.db_uri, args.db_name)

	if args.config:
		app_config = {}
		try:
			import configparser
			config = configparser.ConfigParser()
			config.read(args.config)
			app_config = config["Application"]
		except (ImportError, KeyError):
			import ConfigParser
			config = ConfigParser.ConfigParser()
			config.read(args.config)
			app_config = dict(config.items("Application"))

		if "SessionCookie" in app_config:
			nudelnio.config["session_cookie"] = app_config["SessionCookie"]
		if "LongtimeCookie" in app_config:
			nudelnio.config["longtime_cookie"] = app_config["LongtimeCookie"]
		if "PageSize" in app_config:
			nudelnio.config["page_size"] = int(app_config["PageSize"])
		if "AssetPath" in app_config:
			nudelnio.config["asset_path"] = app_config["AssetPath"]
		if "AllowRegistration" in app_config:
			nudelnio.config["allow_registration"] = app_config.getboolean("AllowRegistration")

	if args.verbose:
		print("Run server")
	server = tornado.httpserver.HTTPServer(nudelnio.app)
	server.add_sockets(sockets)
	tornado.ioloop.IOLoop.instance().start()
