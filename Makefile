# Makefile to create compressed css/js

SCSS := scss
SCSSFLAGS := 

override objects := static/css/application.min.css

.PHONY: all
all: $(objects)

%.min.css: %.css
	$(SCSS) -t compressed $^ $@

.PHONY: clean
clean:
	rm -rf $(objects)
