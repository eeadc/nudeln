# coding: utf-8

import jinja2
import nudelnio.session
import nudelnio

loader = jinja2.PackageLoader('nudelnio', 'views')
env = jinja2.Environment(loader=loader)

def markdown_filter(val):
	import markdown
	return markdown.markdown(val)

env.filters["markdown"] = markdown_filter

def get_session(*elems):
	if len(elems) > 0:
		try:
			sess = nudelnio.session.Session()

			for elem in elems:
				sess = sess[elem]
			return sess
		except KeyError:
			return None

	return nudelnio.session.Session()

env.globals["get_session"] = get_session
env.globals["session"] = get_session

def get_config():
	return nudelnio.config

env.globals["get_config"] = get_config
env.globals["config"] = get_config

def template(name):
	try:
		return env.get_template(name + '.html')
	except:
		return env.get_template(name)

def view(name, **kwargs):

	return template(name).render(**kwargs)

