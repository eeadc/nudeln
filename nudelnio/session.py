# coding: utf-8

import nudelnio
import nudelnio.utils
import bottle
import bson.dbref
import bson.objectid

class Session:
	def __init__(self, config=None):
		self.config = config or nudelnio.config
		if self.config["session_cookie"] in bottle.request.cookies:
			self.id = bottle.request.cookies[self.config["session_cookie"]]
		elif self.config["longtime_cookie"] in bottle.request.cookies:
			self.id = bottle.request.cookies[self.config["longtime_cookie"]]
		else:
			self.id = None

	def install(self):
		bottle.response.set_cookie(self.config["session_cookie"], self.__str__())
		bottle.response.set_cookie(
			self.config["longtime_cookie"],
			self.__str__(),
			max_age=self.config["longtime"],
		)

	def save(self, data):
		if "_id" not in data:
			data["_id"] = nudelnio.utils.generate_session_id()
			self.id = data["_id"]
			self.config["database"].sessions.insert(data)
			self.install()
		else:
			self.config["database"].sessions.update({"_id": self.id}, data)
	
	def __str__(self):
		return str(self.id)

	def dict(self):
		return self.config["database"].sessions.find_one({"_id": self.id}) or {}

	def __getitem__(self, key):
		item = self.dict()[key]
		if type(item) == bson.dbref.DBRef:
			return self.config["database"].dereference(item)
		return item

	def __setitem__(self, key, value):
		data = self.dict()
		data[key] = value

		self.save(data)

	def __delitem__(self, key):
		data = self.dict()
		del data[key]

		self.save(data)

	def __contains__(self, key):
		return key in self.dict()

	def friends(self):
		friends = self.config["database"].users.find({
			"_id": {"$in": self["user"]["friends"]}
		})
		return friends
