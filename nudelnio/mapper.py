# coding: utf-8

from bson.code import Code
from nudelnio import config

import pymongo

post_tags = {
	"map": Code("""
	function() {
		var post = this._id;
		this.tags.forEach(function(tag) {
			emit(tag, { count: 1, posts: [ post ] });
		});
	}
	"""),
	"reduce": Code("""
	function(key, values) {
		var result = { count: 0, posts: [] }
		values.forEach(function(value) {
			result.count += value.count;
			result.posts = result.posts.concat(value.posts);
		});

		return result;
	}
	""")
}

def sort_tags(foo):
	return foo["value"]["count"]

def map_post(post):
	newpost = dict(post)
	newpost["user"] = config["database"].users.find_one({
		"_id": post["user"]
	})

	if "original" in post:
		original = config["database"].posts.find_one({
			"_id": post["original"]
		})
		if original:
			newpost["original"] = map_post(original)
		else:
			del newpost["original"]
	
	if "via" in post:
		via = config["database"].posts.find_one({
			"_id": post["via"]
		})

		if via:
			newpost["via"] = map_post(via)
		else:
			del newpost["via"]
	
	if "origin" in post:
		origin = config["database"].posts.find_one({
			"_id": post["origin"]
		})

		if origin:
			newpost["origin"] = map_post(origin)
		else:
			del newpost["origin"]
	
	return newpost

def map_reduce_post_tags(query={}):
	print("Rereduce {0}".format(query))
	config["database"].posts.map_reduce(
		post_tags["map"],
		post_tags["reduce"],
		{ "reduce": "posts.tags" },
		full_response=True,
		query=query
	)

	return config["database"].posts.tags

def map_post_tags(post_ids):
	tags = list(config["database"].posts.tags.find({
		"value.posts": {"$in": post_ids}
	}).sort([
		("value.count", pymongo.DESCENDING),
		("_id", pymongo.ASCENDING)
	]))

	return tags

def post_created(id):
	map_reduce_post_tags({"_id": id})
