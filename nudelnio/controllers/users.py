# coding: utf-8

import hashlib

from nudelnio.views import view
from nudelnio.utils import hash_password, generate_salt, redirect_referer
from bottle import request, redirect
from nudelnio import config
from bson.dbref import DBRef
import re
import pymongo.errors

from nudelnio.session import Session
import nudelnio.session as session

reserved_keywords = [
	"everyone",
	"friends",
	"sign_up",
	"sign_in",
	"user",
	"logout",
	"post",
	"posts",
	"static",
	"assets",
	"terms_of_use",
	"privacy_policy",
	"contact"
]

def new_session():
	return view("users/new_session", form={})

def new_registration():
	return view("users/new_registration", form={})

def create_session():
	user = config["database"].users.find_one({
		"username": request.forms["username"]
	})
	if not user:
		return view("users/new_session", form=request.forms, info="Invalid username or password")

	password_hash = hash_password(request.forms["password"], user["salt"])
	if user["password_hash"] != password_hash:
		return view("users/new_session", form=request.forms, info="Invalid username or password")

	sess = Session()
	sess["user"] = DBRef("users", user["_id"])

	return redirect("/")

def create_registration():
	if not config["allow_registration"]:
		return redirect("/")

	if  len(request.forms["email"]) == 0 or \
			len(request.forms["username"]) == 0 or \
			len(request.forms["password"]) == 0:
		return view("users/new_registration", form=request.forms, view="Please fill out all fields")

	if request.forms["password"] != request.forms["password_confirmation"]:
		return view("users/new_registration", form=request.forms, info="Password is not equal to confirmation")

	if not re.match("^[0-9a-zA-Z_\-]{3,}$", request.forms["username"]):
		return view("users/new_registration", form=request.forms, info="Username is invalid")

	if request.forms["username"] in reserved_keywords:
		return view("users/new_registration", form=request.forms, info="Username is reserved")

	salt = generate_salt()
	password_hash = hash_password(request.forms["password"], salt)

	config["database"].users.ensure_index("username", unique=True)
	config["database"].users.ensure_index("email", unique=True)
	try:
		id = config["database"].users.insert({
			"username":      request.forms["username"],
			"email":         request.forms["email"],
			"password_hash": password_hash,
			"salt":          salt,
			"friends":       []
		}, safe=True)
	except pymongo.errors.OperationFailure:
		return view("users/new_registration", form=request.forms, info="Username or email already in use")

	sess = Session()
	sess["user"] = DBRef("users", id)

	return redirect("/")

def edit_registration():
	if "user" not in Session():
		return redirect("/sign_in")
	user = Session()["user"]
	return view("users/edit_registration", form=user)

def update_registration():
	if "user" not in Session():
		return redirect("/sign_in")
	user = Session()["user"]

	if len(request.forms.get("password", "")) != 0 and \
		 len(request.forms.get("password_confirmation", "")) != 0:
		if request.forms["password"] != request.forms["password_confirmation"]:
			return view("users/edit_registration", form=request.forms, info="Password is not equal to confirmation")
		user["password_hash"] = hash_password(request.forms["password"], user["salt"])

	if "email" in request.forms:
		user["email"] = request.forms["email"]

	config["database"].users.save(user)

	return redirect("/user/edit")

def destroy_session():
	del Session()["user"]
	return redirect("/")

def friend(username):
	if "user" not in Session():
		return redirect("/sign_in")

	friend = config["database"].users.find_one({"username": username})
	if not friend:
		abort(404, "User not found")

	user = Session()["user"]
	if friend["_id"] not in user["friends"]:
		user["friends"].append(friend["_id"])
		config["database"].users.save(user)
	
	return redirect_referer()

def unfriend(username):
	if "user" not in Session():
		return redirect("/sign_in")

	friend = config["database"].users.find_one({"username": username})
	if not friend:
		abort(404, "User not found")

	user = Session()["user"]
	user["friends"].remove(friend["_id"])
	config["database"].users.save(user)

	return redirect_referer()

def invited():
	return new_registration()
