# coding: utf-8

from nudelnio.views import view
from nudelnio import config

from bottle import redirect, request, response, abort
from nudelnio.session import Session
from nudelnio.utils import save_asset, generate_asset_name, redirect_referer, paginate
from bson.objectid import ObjectId
from bson.dbref import DBRef
from datetime import datetime
import pymongo
import re

import nudelnio.mapper

"""
A simple definition of posts:

post = {
	"type"     : enum(
		"image",
		"video",
		"link",
		"quote",
		"text"
	),
	"title"    : string,
	"text"     : string,

	for "type" == "image" {
		"image"  : file
	}
	for "type" == "video" {
		"url"    : string
	}
	for "type" == "link" {
		"url"    : string
	}
	for "type" == "quote" {
		"source" : string
	}
	for "type" == "text" {
	}

	"user"       : id,

	"original"   : id,
	"via"        : id,
	"is_repost"  : bool,

	"origin"     : id,

	"tags"       : list
	"created_at" : time,
}
"""

def normalize_tags(tags):
	result = []

	for tag in re.split("[\s\;\,]+", tags):
		if len(tag) == 0:
			continue
		result.append(tag.lower())
	
	return result

def collection(cursor, user=None, template="posts/collection"):
	pagination, cursor = paginate(cursor, request.query.get("page", 0))

	posts = []
	post_ids = []
	for post in cursor:
		post = nudelnio.mapper.map_post(post)
		post_ids.append(post["_id"])
		posts.append(post)
	tags = nudelnio.mapper.map_post_tags(post_ids)

	return view(template, posts=posts, user=user, tags=tags, pagination=pagination)

def index(username=None):
	posts = None
	user = None
	if not username:
		if "user" in Session():
			return friends()
		else:
			return everyone()
	else:
		user = config["database"].users.find_one({
			"username": username
		})
		if not user:
			abort(404)
		posts = config["database"].posts.find({
			"user": user["_id"]
		})
	
	posts = posts.sort("created_at", pymongo.DESCENDING)

	return collection(posts, user=user, template="posts/user")

def everyone():
	posts = config["database"].posts.find({
		"is_repost" : { "$ne": True }
	}).sort("created_at", pymongo.DESCENDING)
	return collection(posts)

def friends():
	if "user" not in Session():
		return redirect("/sign_in")

	try:
		friends = Session()["user"]["friends"]
	except KeyError:
		friends = []
	config["database"].posts.ensure_index("user")
	posts = config["database"].posts.find({
		"user": { "$in": friends }
	}).sort("created_at", pymongo.DESCENDING)

	return collection(posts)

def find_by_tag(tag):
	config["database"].posts.ensure_index("tags")
	posts = config["database"].posts.find({
		"tags": tag,
		"is_repost" : { "$ne": True }
	}).sort("created_at", pymongo.DESCENDING)

	return collection(posts)

def new():
	return new_text()

def new_video():
	return ""

def new_text():
	if "user" not in Session():
		return redirect("/sign_in")
	return view("posts/new_text", form={})

def new_image():
	if "user" not in Session():
		return redirect("/sign_in")
	return view("posts/new_image", form={})

def create():
	if "user" not in Session():
		return redirect("/sign_in")
	if request.forms.get("type") not in ["text", "video", "image"]:
		return view("posts/new", form=request.forms, info="Invalid type")

	post = {}
	post["type"] = request.forms["type"]
	post["title"] = request.forms.get("title")
	post["text"] = request.forms.get("text")

	post["user"] = Session()["user"]["_id"]
	post["created_at"] = datetime.now()

	if request.forms.get("origin"):
		post["origin"] = ObjectId(request.forms["origin"])

	post["assets"] = []

	try:
		post["tags"] = normalize_tags(request.forms.get("tags", ""))
		if len(post["tags"]) not in config["tags_range"]:
			raise KeyError()
	except KeyError:
		if "origin" in request.forms:
			pass
		return view("posts/new_{0}".format(post["type"]), form=request.forms, info="Please use at least 2 tags")

	if request.forms["type"] == "image":
		for input_name in request.files:
			file = request.files[input_name]
			
			ext = file.filename.rsplit(".")[-1]
			print(ext)
			if ext not in config["allowed_assets"]:
				continue
			filename = generate_asset_name(file.filename)
			post["assets"].append(filename)

			save_asset(filename, file.file)

	config["database"].posts.ensure_index("tags")
	config["database"].posts.ensure_index("user")
	id = config["database"].posts.save(post)
	nudelnio.mapper.post_created(id)

	return redirect("/post/{0}".format(str(id)))

def show(id):
	post = config["database"].posts.find_one(ObjectId(id))
	if not post:
		abort(404, "Post not found")
	return view("posts/show", post=nudelnio.mapper.map_post(post))

def repost(id):
	if "user" not in Session():
		return redirect("/sign_in")

	post = config["database"].posts.find_one(ObjectId(id))
	if Session()["user"]["_id"] == post["user"]:
		abort(403)

	if "original" not in post:
		post["original"] = post["_id"]
	else:
		post["via"] = post["_id"]

	del post["_id"]
	post["user"] = Session()["user"]["_id"]
	post["created_at"] = datetime.now()
	post["is_repost"] = True
	id = config["database"].posts.insert(post, safe=True)

	return redirect_referer()

def reply(id):
	origin = config["database"].posts.find_one(ObjectId(id))
	return view("posts/reply", origin=nudelnio.mapper.map_post(origin), type=origin["type"])

def destroy(id):
	if "user" not in Session():
		return redirect("/sign_in")
	post = config["database"].posts.find_one(ObjectId(id))

	if post["user"] != Session()["user"]["_id"]:
		abort(403)

	config["database"].posts.remove(ObjectId(id))

	return redirect_referer("/post/")
