# coding: utf-8

import bottle
from nudelnio.views import view
from nudelnio.session import Session

def terms_of_use():
	return view("terms_of_use")

def privacy_policy():
	return view("privacy_policy")

def contact():
	return view("contact")

def index():
	if "user" in Session():
		from .posts import index
		return index()
	return view("index")

