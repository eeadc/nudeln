# coding: utf-8

import hashlib
import hmac
import random

from nudelnio import config

def gp_digest():
	return config["gp_hash"]()

def generate_asset_name(filename):
	hmac_dig = hmac.new(bytes(random.randrange(0, 1023)), digestmod=config["gp_hash"])
	hmac_dig.update(filename.encode())

	extension = filename.rsplit(".")[-1]
	return hmac_dig.hexdigest() + "." + extension

def generate_salt():
	digest = gp_digest()
	digest.update(bytes(random.randrange(0, 1000)))

	return digest.digest()

def generate_session_id():
	digest = gp_digest()
	digest.update(bytes(random.randrange(0, 4294967296)))
	digest.update(digest.digest())
	return digest.hexdigest()

def save_asset(filename, source):
	dest = open(config["asset_path"].format(filename), "wb")
	
	while True:
		chunk = source.read(4096)
		if len(chunk) == 0:
			break
		dest.write(chunk)

def hash_password(password, salt=None):
	if type(password) == str:
		password = password.encode()
	
	if salt:
		hmac_dig = hmac.new(salt, digestmod=config["password_hash"])
		hmac_dig.update(password)

		return hmac_dig.hexdigest()

	dig = config["password_hash"]()
	dig.update(password)

	return dig.hexdigest()

import bottle

def redirect_referer(root_pattern=None):
	import re
	if "referer" in bottle.request.headers:
		if root_pattern and re.search(root_pattern, bottle.request.headers["referer"]):
			return bottle.redirect("/")
		return bottle.redirect(bottle.request.headers["referer"])
	return bottle.redirect("/")

def paginate(cursor, page=0):
	page = int(page)
	elements = cursor.count()
	pages = (elements - 1) // config["page_size"] + 1

	cursor = cursor.limit(config["page_size"])
	cursor = cursor.skip(config["page_size"] * page)

	return { "elements": elements, "pages": pages, "current_page": page }, cursor
