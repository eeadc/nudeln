# coding: utf-8

import tornado.web
import tornado.wsgi
import bottle
import hashlib
import random

config = {
		"database": None,
		"password_hash": hashlib.sha256,
		"session_cookie": "COQIO_SESS",
		"longtime_cookie": "COQIO_LTSESS",
		# General Purpose Hash
		"gp_hash": hashlib.md5,
		"page_size": 10,
		"asset_path": "assets/{0}",
		"allowed_assets": [ "png", "jpg", "jpeg", "gif" ],
		"longtime": 3600 * 24 * 365,
		"tags_range": range(2, 11),
		"allow_registration": True
	}

import nudelnio.controllers.home
import nudelnio.controllers.users
import nudelnio.controllers.posts
import nudelnio.controllers.error

# bottle app
bottle_app = bottle.Bottle()

bottle_app.get   ("/")                   (controllers.home.index)
bottle_app.get   ("/terms_of_use")       (controllers.home.terms_of_use)
bottle_app.get   ("/privacy_policy")     (controllers.home.privacy_policy)
bottle_app.get   ("/contact")            (controllers.home.contact)
bottle_app.get   ("/<username>")         (controllers.posts.index)
bottle_app.get   ("/<username>/friend")  (controllers.users.friend)
bottle_app.get   ("/<username>/unfriend")(controllers.users.unfriend)
bottle_app.get   ("/everyone")           (controllers.posts.everyone)
bottle_app.get   ("/friends")            (controllers.posts.friends)

bottle_app.get   ("/sign_up")            (controllers.users.new_registration)
bottle_app.get   ("/sign_in")            (controllers.users.new_session)
bottle_app.post  ("/sign_up")            (controllers.users.create_registration)
bottle_app.post  ("/sign_in")            (controllers.users.create_session)
bottle_app.get   ("/user/edit")          (controllers.users.edit_registration)
bottle_app.post  ("/user/edit")          (controllers.users.update_registration)
bottle_app.get   ("/logout")             (controllers.users.destroy_session)

bottle_app.get   ("/post")               (controllers.posts.new)
bottle_app.get   ("/post/text")          (controllers.posts.new_text)
bottle_app.get   ("/post/image")         (controllers.posts.new_image)
bottle_app.get   ("/post/video")         (controllers.posts.new_video)
bottle_app.get   ("/post/<id>")          (controllers.posts.show)
bottle_app.post  ("/post")               (controllers.posts.create)
bottle_app.get   ("/post/<id>/destroy")  (controllers.posts.destroy)
bottle_app.get   ("/post/<id>/repost")   (controllers.posts.repost)
bottle_app.get   ("/post/<id>/reply")    (controllers.posts.reply)

bottle_app.get   ("/posts")              (controllers.posts.everyone)
bottle_app.get   ("/posts/tag/<tag>")    (controllers.posts.find_by_tag)

bottle_app.post  ("/invited")            (controllers.users.invited)

# some error setup
# bottle_app.error (500) (controllers.error.internal_server_error)
# bottle_app.error (404) (controllers.error.not_found)
# bottle_app.error (403) (controllers.error.forbidden)

# tornado app
app = tornado.web.Application([
	(r'/static/(.*)', tornado.web.StaticFileHandler, dict(path='static/')),
# TODO Write an AssetsHandler to serve assets fast and reliable
#	(r'/assets/(.*)', AssetsHandler),
	(r'/assets/(.*)', tornado.web.StaticFileHandler, dict(path='assets/')),
	(r'.*', tornado.web.FallbackHandler, dict(
		fallback=tornado.wsgi.WSGIContainer(bottle_app)
	))
])

